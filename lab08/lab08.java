public class lab08 {
    public static void main(String[] args)
    {
        int[] randomNumbers = new int[100]; //create two arrays each with 100 elements
        int[] occurances = new int[100];

        for(int i=0; i<randomNumbers.length;i++) //assigns a random number 0 - 99 to every element in randomNumbers
        {
            randomNumbers[i] = (int)(Math.random()*100);
        }
        for(int i=0; i<occurances.length; i++) //loops through each element in occurences
        {
            int cnt =0;//initialize a counter to zero for every new element
            for(int j=0; j<occurances.length; j++) //loops through each element in occurances again
            {
                if(i == randomNumbers[j]) //if number in random numbers is equal to element number in occurance, signify you have found an occurance of that number
                    cnt++; //increase counter
            }
            occurances[i] = cnt; //set element to the number of times that number occured
            if(occurances[i] == 1)//use non plural time if only one occurance
                System.out.println(i +" occurs "+ occurances[i] +" time");
            else //use plural form for when 0 or more than 1 occrance
                System.out.println(i +" occurs "+ occurances[i] +" times");
        }
    } //end main method
} //end class