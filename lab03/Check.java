/* by Joshua Krinsky 9.14.18
the goal of check is to take in user input and calculate to split the bill evenly
the input is how much dinner cost, the percent tip, and how many people ate which the user inputs
the out put is evenly how much each person must pay
*/

import java.util.Scanner; //imports scanner to allow user input

public class Check{
  public static void main (String [] args) //main method
  {
    Scanner reader = new Scanner(System.in); //declares scanner
    //prompts user for amount of check
    System.out.println("Enter the original cost of the check in the form XX.XX");
    //records data
    double checkTotal = reader.nextDouble();
    
    //prompts user for the percentage you want to tip
    System.out.println("Enter the percentage tip you wish to pay as a whole number XX");
    //records data
    double tipAmount = reader.nextInt();
    //converts to a percentage
    tipAmount /= 100;
    
    //prompts user for how many people went to dinner
    System.out.println("Enter the number of people who went out to dinner");
    //records data
    int numPeople = reader.nextInt();
    
    double totalCost = checkTotal * (1+tipAmount); //gets checkTotal and applies tip
    double costPerPerson = totalCost/numPeople; //finds cost per person
    
    //splits into three catagories dollars dimes and pennies
    int dollars = (int)costPerPerson; //whole number of the costPerPerson
    int dimes = (int)(costPerPerson * 10) %10; //tenths column of the cost
    int pennies = (int)(costPerPerson * 100) %10; //hundreths column of the cost
    
    //displays the amount each person must pay
    System.out.println("Each person in the group owns $"+dollars+"."+dimes+pennies);
  } //end main method
  
}//end class