/* By joshua Krinsky 9.21.18
either takes the users input or generates to random numbers and 
then outputs it term in Craps using if statements
*/

import java.util.Random; //import Random
import java.util.Scanner; //import Scanner
public class CrapsIf{
  public static void main(String[] args){ //begin main method
    Scanner reader = new Scanner(System.in); //declare Scanner
    
    System.out.println("Would you like to randomly cast dice (1) or state the two dice" +
                       "you want to evalute (2)"); //ask user if they want input or random numbers
    
    int choice = reader.nextInt(); //record input
    int diceVal1 = 0; //dice value
    int diceVal2 = 0; //dice value
    if(choice == 1) //if statement for choice
    {//if they want random numbes
      Random rand = new Random();//declare random generator
        //set dice 1 and dice two too random numbers
      diceVal1 = rand.nextInt(6)+1;
      diceVal2 = rand.nextInt(6)+1;
    }
    else if (choice == 2)//if the user wants to input
    {
        System.out.println("Input value 1"); //ask for input
        diceVal1 = reader.nextInt(); //record input
        System.out.println("Input value 2"); //ask for input again
        diceVal2 = reader.nextInt(); //record second input for dice 2
        //checks to ensure that the values are between 1 and 6 inclusive
        if ((diceVal1 > 6 || diceVal1 < 1) || (diceVal2 > 6 || diceVal2 < 1))
        {
          System.out.println("Either val 1 or val 2 is not a valid input");
          System.exit(-1);
        }
    }
    else
    { //if the user doesn't input one or two
      System.out.println("Not a valid input");
      System.exit(-1);
    }
    //if statements to decide Craps term name then displays it
    /*these if statements works by deciding the value of the first dice then
    evaluating the value of the second and printing out the corrisponding statment*/
    if(diceVal1 == 1) //dice one value is 1
    {
      if(diceVal2 == 1)//dice 2 value is 1
        System.out.println("Snake Eyes");
       else if(diceVal2 == 2)//dice 2 value is 2
         System.out.println("Ace Deuce");
      else if (diceVal2 == 3)//dice 2 value is 3
        System.out.println("Easy Four");
      else if(diceVal2 == 4)//dice 2 value is 4
        System.out.println("Fever Five");
      else if(diceVal2 == 5)//dice 2 value is 5
        System.out.println("Easy Six");
      else if (diceVal2 == 6)//dice 2 value is 6
        System.out.println("Seven Out");
    }
    else if(diceVal1 == 2)//dice one value is 2
    {
      if(diceVal2 == 1)
        System.out.println("Ace Deuce");
       else if(diceVal2 == 2)
         System.out.println("Hard Four");
      else if (diceVal2 == 3)
        System.out.println("Fever Five");
      else if(diceVal2 == 4)
        System.out.println("Easy Six");
      else if(diceVal2 == 5)
        System.out.println("Seven Out");
      else if (diceVal2 == 6)
        System.out.println("Easy Eight");
    }
    else if(diceVal1 == 3) //dice one value is 4
    {
      if(diceVal2 == 1)
        System.out.println("Easy Four");
       else if(diceVal2 == 2)
         System.out.println("Fever Five");
      else if (diceVal2 == 3)
        System.out.println("Hard Six");
      else if(diceVal2 == 4)
        System.out.println("Seven Out");
      else if(diceVal2 == 5)
        System.out.println("Easy Eight");
      else if (diceVal2 == 6)
        System.out.println("Nine");
    }
    else if(diceVal1 == 4)//dice one value is 4
    {
      if(diceVal2 == 1)
        System.out.println("Fever Five");
       else if(diceVal2 == 2)
         System.out.println("Easy Six");
      else if (diceVal2 == 3)
        System.out.println("Seven Out");
      else if(diceVal2 == 4)
        System.out.println("Hard Eigth");
      else if(diceVal2 == 5)
        System.out.println("Nine");
      else if (diceVal2 == 6)
        System.out.println("Easy Ten");
    }
    else if(diceVal1 == 5)//dice one value is 5
    {
      if(diceVal2 == 1)
        System.out.println("Easy Six");
       else if(diceVal2 == 2)
         System.out.println("Seven Out");
      else if (diceVal2 == 3)
        System.out.println("Easy Eight");
      else if(diceVal2 == 4)
        System.out.println("Nine");
      else if(diceVal2 == 5)
        System.out.println("Hard Ten");
      else if (diceVal2 == 6)
        System.out.println("Yo-Leven");
    }
    else if(diceVal1 == 6)//dice one value is 6
    {
      if(diceVal2 == 1)
        System.out.println("Seven Out");
       else if(diceVal2 == 2)
         System.out.println("Easy Eight");
      else if (diceVal2 == 3)
        System.out.println("Nine");
      else if(diceVal2 == 4)
        System.out.println("Easy Ten");
      else if(diceVal2 == 5)
        System.out.println("Yo-Leven");
      else if (diceVal2 == 6)
        System.out.println("Boxcars");
    }
    else{ //if value doesn't fit any
      System.out.println("Error");
    }
  } // end main method
} //end class