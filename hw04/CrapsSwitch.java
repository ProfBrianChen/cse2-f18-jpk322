/* By joshua Krinsky 9.21.18
either takes the users input or generates to random numbers and 
then outputs it term in Craps using switch statements
*/
import java.util.Scanner; //import scanner
import java.util.Random; //import Random
public class CrapsSwitch{
  public static void main(String[] args){ //start main class
      Scanner reader = new Scanner(System.in); //declare scanner
    
    System.out.println("Would you like to randomly cast dice (1) or state the two dice" +
                       "you want to evalute (2)"); //ask user if they want input or random numbers
    
    int choice = reader.nextInt(); //record input
    int diceVal1 = 0; //dice value
    int diceVal2 = 0; //dice value
    switch (choice) //switch statement for choice
    {
      case 1: //if they want random numbes
        Random rand = new Random(); //declare random generator
        //set dice 1 and dice two too random numbers
        diceVal1 = rand.nextInt(6)+1;
        diceVal2 = rand.nextInt(6)+1;
        break; //break out
      case 2: //if the user wants to input
        System.out.println("Input value 1"); //ask for input
        diceVal1 = reader.nextInt(); //record input
        System.out.println("Input value 2"); //ask for input again
        diceVal2 = reader.nextInt(); //record second input for dice 2
        //checks to ensure that the values are between 1 and 6 inclusive
        switch(diceVal1)
        {
          case 1:
          case 2:
          case 3:
          case 4:
          case 5:
          case 6:
            break;//1-6 are good anything else exits the program
          default:
            System.out.println("This is not a valid input for Dice 1");
            System.exit(-1);
            break;
        }
        switch(diceVal2)
        {
          case 1:
          case 2:
          case 3:
          case 4:
          case 5:
          case 6:
            break;//same here as for diceVal1
          default:
            System.out.println("This is not a valid input for Dice 2");
            System.exit(-1);
            break;
        }
        break;
      default: //if the user doesn't input 1 or 2
        System.out.println("Not a valid input");
        System.exit(-1);
    }//switch statements to decide Craps term name then displays it
    /*these switch statements works by deciding the value of the first dice then
    evaluating the value of the second and printing out the corrisponding statment*/
    switch(diceVal1){
      case 1: //if dice 1's value is one
        switch(diceVal2){
          case 1: //dice 2 value is 1
            System.out.println("Snake Eyes");
            break;
          case 2://dice 2 value is 2
            System.out.println("Ace Deuce");
            break;
          case 3://dice 2 value is 3
            System.out.println("Easy Four");
            break;
          case 4://dice 2 value is 4
            System.out.println("Fever Five");
            break;
          case 5://dice 2 value is 5
            System.out.println("Easy Six");
            break;
          case 6://dice 2 value is 6
            System.out.println("Seven Out");
            break;
        }
        break;
    case 2: //dice one value is 2
        switch(diceVal2)
        {
          case 1:
            System.out.println("Ace Deuce");
            break;
          case 2:
            System.out.println("Hard Four");
            break;
          case 3:
            System.out.println("Fever Five");
            break;
          case 4:
            System.out.println("Easy Six");
            break;
          case 5:
            System.out.println("Seven Out");
            break;
          case 6:
            System.out.println("Easy Eight");
            break;
        }
        break;
        
      case 3://dice one value is 3
        switch(diceVal2)
        {
          case 1:
            System.out.println("Easy Four");
            break;
          case 2:
            System.out.println("Fever Five");
            break;
          case 3:
            System.out.println("Hard Six");
            break;
          case 4:
            System.out.println("Seven Out");
            break;
          case 5:
            System.out.println("Easy Eight");
            break;
          case 6:
            System.out.println("Nine");
            break;
        }
        break;
      case 4://dice one value is 4
        switch(diceVal2){
          case 1:
            System.out.println("Fever Five");
            break;
          case 2:
            System.out.println("Easy Six");
            break;
          case 3:
            System.out.println("Seven Out");
            break;
          case 4:
            System.out.println("Hard Eigth");
            break;
          case 5:
            System.out.println("Nine");
            break;
          case 6:
            System.out.println("Easy Ten");
            break;
        }
      break;
      case 5://dice one value is 5
        switch(diceVal2)
        {
          case 1:
            System.out.println("Easy Six");
            break;
          case 2:
            System.out.println("Seven Out");
            break;
          case 3:
            System.out.println("Easy Eight");
            break;
          case 4:
            System.out.println("Nine");
            break;
          case 5:
            System.out.println("Hard Ten");
            break;
          case 6:
            System.out.println("Yo-Leven");
            break;
            
        }
        break;
      case 6://dice one value is 6
        switch(diceVal2)
        {
          case 1:
            System.out.println("Seven Out");
            break;
          case 2:
            System.out.println("Easy Eight");
            break;
          case 3:
            System.out.println("Nine");
            break;
          case 4:
            System.out.println("Easy Ten");
            break;
          case 5:
            System.out.println("Yo-Leven");
            break;
          case 6:
            System.out.println("Boxcars");
            break;
        }
        break;
      default: //if not 1-6 
        System.out.println("Error");
        break;
    }//end diceVal1 switch
  } // end main method
} //end class