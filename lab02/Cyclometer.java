//created by joshua Krinsky 9.7.18 
// the purpose of Cyclometer is to record time elapsed and the number of rotations of the front wheel for two trips
// it will then output minutes for each trip, rotations in each trip, distance of each trip and total distance
public class Cyclometer{
  public static void main (String args[])
  {
    int secsTripOne =480; //seconds of first trip
    int secsTripTwo = 3220; //seconds of seconds trip
    int countsTripOne = 1561;//rotations of wheel in first trip
    int countsTripTwo = 9037; //rotations fo wheel in seconds trip
    
    final double WHEEL_DIAMETER = 27.0; //Diameter of the wheel
    final double PI = 3.14159; //math constant PI
    final double FEET_PER_MILE = 5280.0;
    final double INCHES_PER_FOOT = 12.0;
    final double SECONDS_PER_MINUTE = 60.0;
    double distanceTripOne, distanceTripTwo, distanceTotal; //distance one distance two and distance total
    
    System.out.println("Trip one took " + (secsTripOne/SECONDS_PER_MINUTE) + " minutes and had "+countsTripOne+" counts");
    //display trip one ^ and trip two v telling the user minutes of the ride and number of roations the wheel made
    System.out.println("Trip two took " + (secsTripTwo/SECONDS_PER_MINUTE) + " minutes and had "+countsTripTwo+" counts");
    
   distanceTripOne = (countsTripOne * WHEEL_DIAMETER * PI) /(INCHES_PER_FOOT * FEET_PER_MILE); // calculates the distance of trip one in miles
   distanceTripTwo = (countsTripTwo * WHEEL_DIAMETER * PI) /(INCHES_PER_FOOT * FEET_PER_MILE); // calculates the distance of trip two in miles
   distanceTotal = distanceTripOne + distanceTripTwo;
    
   //Prints the distances of trip one two and total
  System.out.println("Trip 1 was "+distanceTripOne+" miles");
	System.out.println("Trip 2 was "+distanceTripTwo+" miles");
	System.out.println("The total distance was "+distanceTotal+" miles");

    
  }
  
}