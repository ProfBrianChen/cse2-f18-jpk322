/* by Joshua Krinsky 10.12.18
* takes in user input and prints out a pttern according to number of rows
* 1
* 12
* 123
* 1234
* */
import java.util.Scanner; //import scanner
public class PatternA {//start class
    public static void main(String args[]) //start main method
    {
        int numbRows; //keeps track of number of rows
        Scanner myScanner = new Scanner(System.in); //declare scanner
        while(true) { //while loop to ensure user enters correct input
            System.out.println("Please input a number form 1-10");
            boolean correctValue = myScanner.hasNextInt();
            if (correctValue) {//if int is inputted
                numbRows = myScanner.nextInt();
                if (numbRows < 1 || numbRows > 10) {//check within bounds
                    continue; //go to top of loop
                } else {
                    break; //break out
                }
            } else {
                myScanner.next();
            }
        }
            for(int i=1; i<=numbRows; i++) { //print pattern
                for (int j = 1; j <= i; j++) {
                    System.out.print(j+" ");
                }
                System.out.println();
            }
    }
}
