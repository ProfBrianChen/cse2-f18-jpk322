/* by Joshua Krinsky 10.12.18
 * takes in user input and output pattern
 * 123456
 * 12345
 * 1234
 * 123
 * 12
 * 1
 * */
import java.util.Scanner;

public class PatternB {
    public static void main(String args[])
    {
        int numbRows;
        Scanner myScanner = new Scanner(System.in);
        while(true) {
            System.out.println("Please input a number form 1-10");
            boolean correctValue = myScanner.hasNextInt();
            if (correctValue) {
                numbRows = myScanner.nextInt();
                if (numbRows < 1 || numbRows > 10) {
                    continue;
                } else {
                    break;
                }
            } else {
                myScanner.next();
            }
        }
        for(int i=0; i<numbRows; i++) {
            for (int j = 1; j <= numbRows - i; j++) {
                System.out.print(j+" ");
            }
            System.out.println();
        }
    }
}
