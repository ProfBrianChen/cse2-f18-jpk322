/* by Joshua Krinsky 10.12.18
 * takes in user input and output pattern
 *     1
 *    21
 *   321
 *  4321
 * 54321
 * */
import java.util.Scanner;

public class PatternC {
    public static void main(String args[])
    {
        int numbRows;
        Scanner myScanner = new Scanner(System.in);
        while(true) {
            System.out.println("Please input a number form 1-10");
            boolean correctValue = myScanner.hasNextInt();
            if (correctValue) {
                numbRows = myScanner.nextInt();
                if (numbRows < 1 || numbRows > 10) {
                    continue;
                } else {
                    break;
                }
            } else {
                myScanner.next();
            }
        }
        for(int i=1; i<=numbRows; i++) {
            for (int j = 0; j < numbRows - i; j++) {
                System.out.print(" ");
            }
            for(int k=numbRows; k>0; k--)
            {
                if(k <= i) {
                    System.out.print(k);
                }
            }
            System.out.println();
        }
    }
}
