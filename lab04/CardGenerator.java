/*By Joshua Krinsky 9.21.18
CardGenerator is designed to pick a card at random and then tell you the
vaue and suit of that card
0-12 is diamnods
13-25 is Hearts
26-38 is Spades
39-51 is Clubs
*/

import java.util.Random; //import random
public class CardGenerator{
  public static void main(String[] args) //start main class
  {
    Random rand = new Random(); //import random generator
    int card = rand.nextInt(52); //pick random number between 0 - 51 (Inclusive)
    String cardType; //assigned to card suit
    String cardVal; //assigned card value
    
    if(card <=12) //between 0 and 12
      cardType = "Diamonds"; //assigned suit is diamnds
    else if(card <= 25) //between 13 and 25
      cardType = "Hearts"; //assigned suit is hearts
    else if (card <=38 ) //between 26 and 38
      cardType = "Spades"; //assigned suit is diamonds
    else //between 39 and 51
      cardType = "Clubs"; //assigned suit is diamonds
    
    switch (card % 13){ //find the remainder of the card so I don't need to evaluate for every value
      case 0:
        cardVal = "Ace"; //assigned value ace
        break;
      case 1:
        cardVal = "Two"; //assigned value two
        break;
      case 2:
        cardVal = "Three"; //assigned value three
        break;
      case 3:
        cardVal = "Four"; //assigned value four
        break;
      case 4:
        cardVal = "Five"; //assigned value five
        break;
      case 5:
        cardVal = "Six"; //assigned value six
        break;
      case 6:
        cardVal = "Seven"; //assigned value seven
        break;
      case 7:
        cardVal = "Eight"; //assigned value eight
        break;
      case 8:
        cardVal = "Nine"; //assigned value nine
        break;
      case 9:
        cardVal = "Ten"; //assigned value ten
        break;
      case 10:
        cardVal = "Jack"; //assigned value Jack
        break;
      case 11:
        cardVal = "Queen"; //assigned value queen
        break;
      case 12:
        cardVal = "King"; //assigned value king
        break;
      default:
        cardVal = "Invalid"; //assigned to no value error
        break;
    } //end switch statement
    System.out.println("You picked the "+cardVal +" of "+cardType); // displays card value and suit type
  } //end main method
} //end class
