//By Joshua Krinsky 9.7.18
//input the number of pants, shirts and belts bought
//output the total cost of each item, the total tax on each item
//the cost pre tax, the total tax for the entire purchase and the entire cost of the purchase w/ tax
//after adjusting the tax of each item to only have 2 decimal places everything else fell into places
//however if I was suppose to do the same function for all the outputs I apologize

public class Arithmatic{
  public static void main(String args[])
  {
    //Number of pairs of pants
    int numPants = 3;
    //Cost per pair of pants
    double pantsPrice = 34.98;

    //Number of sweatshirts
    int numShirts = 2;
    //Cost per shirt
    double shirtPrice = 24.99;

    //Number of belts
    int numBelts = 1;
    //cost per belt
    double beltCost = 33.99;

    //the tax rate
    double paSalesTax = 0.06;
    
    //calculates the total cost of pants, shits and belts individually
    double totalCostOfPants = numPants * pantsPrice;
    double totalCostOfShirts = numShirts * shirtPrice;
    double totalCostOfBelts = numBelts * beltCost;
    
    
    //prints the total price of each item
    System.out.println("Total cost of pants " +totalCostOfPants);
    System.out.println("Total cost of shirts " +totalCostOfShirts);
    System.out.println("Total cost of belts " +totalCostOfBelts);
    
    //calculates sales tax of each type of item too two decimal places (i.e belts, pants, shirts)
    double taxOfPants = (int)(totalCostOfPants * paSalesTax * 100);
    taxOfPants /= 100;
    double taxOfShirts = (int)(totalCostOfShirts * paSalesTax *100);
    taxOfShirts /=100;
    double taxOfBelts = (int)(totalCostOfBelts * paSalesTax * 100);
    taxOfBelts /=100;
    
    //displays the sales tax of each type of clothing
    System.out.println("Tax on pants " + taxOfPants);
    System.out.println("Tax on shirts " +taxOfShirts);
    System.out.println("Tax on belts " +taxOfBelts);
    
    //calculate the total price of the purchase without tax
    double totalPriceOfPurchase = totalCostOfPants + totalCostOfShirts + totalCostOfBelts;
    
    //displays the total cost of purchase w/o tax
    System.out.println("Cost of purchase pre-tax "+totalPriceOfPurchase);
    
    //calculate total tax of all items
    double totalCostOfTax = (int)((taxOfPants + taxOfShirts + taxOfBelts) *100);
    totalCostOfTax /=100;
    
    //display total tax of all items
    System.out.println("Total tax "+totalCostOfTax);
    
    //calculate total price of items including tax
    double allCost = totalPriceOfPurchase + totalCostOfTax;
    
    //display total value
    System.out.println("Total price of purchase "+allCost);
    
  }
}