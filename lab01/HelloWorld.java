//By joshua Krinsky 8.31.18
//Hello World class is designed to print the words hello world to the screen

public class HelloWorld
{
  public static void main (String args[])
  {
    //prints hello world to the terminal
    System.out.println("Hello, World");
  }
}