/* By Joshua Krinsky 10.7.18
user inputs how many times they  want to be dealt a hand
the code then deals five cards assigns values and suits checks to ensure that
there are no duplicates and evaulates the hand as either a 4 or 
3 of a kind or full house or two pair or pair
then outputs the the percentage of times each occurred

p.s can't wait until we learn arrays this would've been so much easier with arrays
*/

import java.util.Scanner; //import scanner
import java.util.Random; //import random
public class hw05 { //start class
    public static void main(String args[]) //start main method
    {
        int counter =0; //counts the number of interations
        int handGenerators = 0; //keeps track of the number of hands that should be dealt
        int pairCnter =0; //counter the number of pairs
        int twoPairCnter =0; //counts the number of two pairs
        int threeOfKindCnter = 0; //counts the number of three of a kind
        int fourOfKindCnter =0; //counts the number of four of a kind
        Scanner reader = new Scanner(System.in); //initialize scanner
        Random generator = new Random(); //initialize
        int cardVal1; //card value for the first card
        int cardVal2; //second
        int cardVal3; //thrid
        int cardVal4;//fourth
        int cardVal5;//fifth
        String cardSuit1; //suit for the first card
        String cardSuit2;//second
        String cardSuit3;//etc..
        String cardSuit4;
        String cardSuit5;
        while(true) {//keeps user in inifite loop until they enter an integer
            System.out.println("how many times would you like to generate a hand?"); //prompts user
            boolean correctInput = reader.hasNextInt(); //cecks int is unputted
            if (correctInput) {
                handGenerators = reader.nextInt(); //stores number of hands to be dealt
                break;//breaks out
            }
            else {
                System.out.println("In correct input please use a integer"); //teels user they inputted non int
                reader.next();//clears line
            }
        }
        while(counter < handGenerators) //stays in while loop util all hands have been generated and evaluated
        {
                int card = generator.nextInt(52); //generate card 1 value and suit
                cardVal1 = (card %13);
                if(card <=12)
                    cardSuit1 = "Diamonds";
                else if (card <= 25)
                    cardSuit1  = "Clubs";
                else if (card <=38)
                    cardSuit1  = "Hearts";
                else
                    cardSuit1  = "Spades";

                card = generator.nextInt(52); //generate card two
                cardVal2 = (card %13);
                if(card <=12)
                    cardSuit2 = "Diamonds";
                else if (card <= 25)
                    cardSuit2  = "Clubs";
                else if (card <=38)
                    cardSuit2  = "Hearts";
                else
                    cardSuit2  = "Spades";

                card = generator.nextInt(52); //card three
                cardVal3 = (card %13);
                if(card <=12)
                    cardSuit3 = "Diamonds";
                else if (card <= 25)
                    cardSuit3  = "Clubs";
                else if (card <=38)
                    cardSuit3  = "Hearts";
                else
                    cardSuit3  = "Spades";

                card = generator.nextInt(52); //card four
                cardVal4 = (card %13);
                if(card <=12)
                    cardSuit4 = "Diamonds";
                else if (card <= 25)
                    cardSuit4  = "Clubs";
                else if (card <=38)
                    cardSuit4  = "Hearts";
                else
                    cardSuit4  = "Spades";

                card = generator.nextInt(52); //card five
                cardVal5 = (card %13);
                if(card <=12)
                    cardSuit5 = "Diamonds";
                else if (card <= 25)
                    cardSuit5  = "Clubs";
                else if (card <=38)
                    cardSuit5  = "Hearts";
                else
                    cardSuit5  = "Spades";

            while(cardVal1 == cardVal2 && cardSuit1.equals(cardSuit2)) //ensures 1 and 2 are different
            {
                card = generator.nextInt(52);
                cardVal1 = (card %13);
                if(card <=12)
                    cardSuit1 = "Diamonds";
                else if (card <= 25)
                    cardSuit1  = "Clubs";
                else if (card <=38)
                    cardSuit1  = "Hearts";
                else
                    cardSuit1  = "Spades";
            }
            while(cardVal1 == cardVal3 && cardSuit1.equals(cardSuit3)) //1 and 3
            {
                card = generator.nextInt(52);
                cardVal1 = (card %13);
                if(card <=12)
                    cardSuit1 = "Diamonds";
                else if (card <= 25)
                    cardSuit1  = "Clubs";
                else if (card <=38)
                    cardSuit1  = "Hearts";
                else
                    cardSuit1  = "Spades";
            }
            while(cardVal1 == cardVal4 && cardSuit1.equals(cardSuit4)) //1 and 4
            {
                card = generator.nextInt(52);
                cardVal1 = (card %13);
                if(card <=12)
                    cardSuit1 = "Diamonds";
                else if (card <= 25)
                    cardSuit1  = "Clubs";
                else if (card <=38)
                    cardSuit1  = "Hearts";
                else
                    cardSuit1  = "Spades";
            }
            while(cardVal1 == cardVal5 && cardSuit1.equals(cardSuit5)) //1 and 5
            {
                card = generator.nextInt(52);
                cardVal1 = (card %13);
                if(card <=12)
                    cardSuit1 = "Diamonds";
                else if (card <= 25)
                    cardSuit1  = "Clubs";
                else if (card <=38)
                    cardSuit1  = "Hearts";
                else
                    cardSuit1  = "Spades";
            }
            while(cardVal2 == cardVal3 && cardSuit2.equals(cardSuit3)) //2 and 3
            {
                card = generator.nextInt(52);
                cardVal2 = (card %13);
                if(card <=12)
                    cardSuit2 = "Diamonds";
                else if (card <= 25)
                    cardSuit2  = "Clubs";
                else if (card <=38)
                    cardSuit2  = "Hearts";
                else
                    cardSuit2  = "Spades";
            }
            while(cardVal2 == cardVal4 && cardSuit2.equals(cardSuit4))//2 and 4
            {
                card = generator.nextInt(52);
                cardVal2 = (card %13);
                if(card <=12)
                    cardSuit2 = "Diamonds";
                else if (card <= 25)
                    cardSuit2  = "Clubs";
                else if (card <=38)
                    cardSuit2  = "Hearts";
                else
                    cardSuit2  = "Spades";
            }
            while(cardVal2 == cardVal5 && cardSuit2.equals(cardSuit5)) //2 and 5
            {
                card = generator.nextInt(52);
                cardVal2 = (card %13);
                if(card <=12)
                    cardSuit2 = "Diamonds";
                else if (card <= 25)
                    cardSuit2  = "Clubs";
                else if (card <=38)
                    cardSuit2  = "Hearts";
                else
                    cardSuit2  = "Spades";
            }
            while(cardVal3 == cardVal4 && cardSuit3.equals(cardSuit4)) //3 and 4
            {
                card = generator.nextInt(52);
                cardVal3 = (card %13);
                if(card <=12)
                    cardSuit3 = "Diamonds";
                else if (card <= 25)
                    cardSuit3  = "Clubs";
                else if (card <=38)
                    cardSuit3  = "Hearts";
                else
                    cardSuit3  = "Spades";
            }
            while(cardVal3 == cardVal5 && cardSuit3.equals(cardSuit5))//3 and 5
            {
                card = generator.nextInt(52);
                cardVal3 = (card %13);
                if(card <=12)
                    cardSuit3 = "Diamonds";
                else if (card <= 25)
                    cardSuit3  = "Clubs";
                else if (card <=38)
                    cardSuit3  = "Hearts";
                else
                    cardSuit3  = "Spades";
            }
            while(cardVal4 == cardVal5 && cardSuit4.equals(cardSuit5)) //4 and 5
            {
                card = generator.nextInt(52);
                cardVal4 = (card %13);
                if(card <=12)
                    cardSuit4 = "Diamonds";
                else if (card <= 25)
                    cardSuit4  = "Clubs";
                else if (card <=38)
                    cardSuit4  = "Hearts";
                else
                    cardSuit4  = "Spades";
            }
            //ensures that all the cards are different values^^^^
            //evaluates and determineds what kind of hand was dealt
            if((cardVal1 == cardVal2 && cardVal1 == cardVal3 && cardVal1 == cardVal4) ||
                    (cardVal1 == cardVal3 && cardVal1 == cardVal4 && cardVal1 == cardVal5) ||
                    (cardVal1 == cardVal2 && cardVal1 == cardVal4 && cardVal1 == cardVal5) ||
                    (cardVal1 == cardVal2 && cardVal1 == cardVal3 && cardVal1 == cardVal5) ||
                    (cardVal2 ==cardVal3 && cardVal2 == cardVal4 && cardVal2 == cardVal5))
            { //evaluates if its a four of a kind
                fourOfKindCnter++;
            }
            else if((cardVal1 == cardVal2 && cardVal1 == cardVal3) || (cardVal1 == cardVal2 && cardVal1 == cardVal4) ||
                    (cardVal1 == cardVal2 && cardVal1 == cardVal5) || (cardVal1 == cardVal3 && cardVal1 == cardVal5)||
                    (cardVal1 == cardVal4 && cardVal1 == cardVal5) || (cardVal2 == cardVal3 && cardVal2 == cardVal4)||
                    (cardVal2 == cardVal4 && cardVal2 == cardVal5) || (cardVal2 == cardVal3 && cardVal2 == cardVal5)||
                    (cardVal3 == cardVal4 && cardVal3 == cardVal5))
            {//three of a kind
                threeOfKindCnter++;
                if((cardVal1 == cardVal2 && cardVal1 != cardVal3 && cardVal1 != cardVal4 && cardVal1 != cardVal5) ||
                        (cardVal1 == cardVal3 && cardVal1 != cardVal2 && cardVal1 != cardVal4 && cardVal1 != cardVal5) ||
                        (cardVal1 == cardVal4 && cardVal1 != cardVal2 && cardVal1 != cardVal3 && cardVal1 != cardVal5) ||
                        (cardVal1 == cardVal5 && cardVal1 != cardVal3 && cardVal1 != cardVal4 && cardVal1 != cardVal2) ||
                        (cardVal2 == cardVal3 && cardVal2 != cardVal4 && cardVal2 != cardVal5) ||
                        (cardVal2 == cardVal4 && cardVal2 != cardVal3 && cardVal2 != cardVal5) ||
                        (cardVal2 == cardVal5 && cardVal2 != cardVal3 && cardVal2 != cardVal4) ||
                        (cardVal3 == cardVal4 && cardVal3 != cardVal5) || (cardVal3 == cardVal5 && cardVal3 != cardVal4) ||
                        (cardVal4 == cardVal5))
                { //full house
                    pairCnter++;
                }
            }
            else if((cardVal1 == cardVal2 && cardVal3 == cardVal4) || (cardVal1 == cardVal3 && cardVal2 ==4)||
                    (cardVal1 == cardVal4 && cardVal2 == cardVal3) || (cardVal1 == cardVal5 && cardVal2 == cardVal3) ||
                    (cardVal1 == cardVal5 && cardVal2 == cardVal4) || (cardVal1== cardVal5 && cardVal3 == cardVal4) ||
                    (cardVal2 == cardVal3 && cardVal5 == cardVal4) || (cardVal2 == cardVal4 && cardVal3 == cardVal5) ||
                    (cardVal2 == cardVal5 && cardVal3 == cardVal4))
            {//two pair
                twoPairCnter++;
            }
            else if((cardVal1 == cardVal2) || (cardVal1 == cardVal3) || (cardVal1 == cardVal4) || (cardVal1 == cardVal5) ||
                    (cardVal2 == cardVal3) || (cardVal2 == cardVal4 ) || (cardVal2 == cardVal5 ) ||
                    (cardVal3 == cardVal4) || (cardVal3 == cardVal5) ||
                    (cardVal4 == cardVal5)) {//evaluates if it is a pair
                pairCnter++;
            }
            //incriments counter
            counter++;
        }
        //displays the percentage of time a type of hand showed up
        double pairPercent = pairCnter/(double)counter;
        double twoPairPercent = twoPairCnter/(double)counter;
        double threePairPercent = threeOfKindCnter/(double)counter;
        double fourPairPercent = fourOfKindCnter/(double)counter;
        System.out.println("The number of loops: "+handGenerators+"\n" +
                "The probability of Four-of-a-kind: "+fourPairPercent+"\n" +
                "The probability of Three-of-a-kind: "+threePairPercent+"\n" +
                "The probability of Two-pair: "+twoPairPercent+"\n" +
                "The probability of One-pair: "+pairPercent+"\n");
    }
}