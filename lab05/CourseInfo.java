/* by Joshua Krinsky 10.4.18
takes in the users
  teacher
  course number
  dept name
  times the class meets
  num students in class
  and start time
 only accepts inputs if they fit with the variable type otherwise it stays in a while loop 
 and continues to prompt the user for an input
 then outputs all the information with no fear of error due to mismatchinput types
*/
import java.util.Scanner; //import scanner

public class CourseInfo {//begin class
  public static void main(String args[])
  {  //begin main method
    String teacher; //set up all variables that need to be returned
    int courseNumber;
    String deptName;
    int timesMet;
    int numStudents;
    double startTime;
    
    Scanner myScanner = new Scanner(System.in); //declare scanner
    
    while(true) //enter while loop
    {
      System.out.println("please input your teachers name."); //request input
      boolean correctInput = myScanner.hasNext(); //check if input is a string
      
      if(correctInput) //if correct record the input
      {
        teacher = myScanner.next(); //record input for teacher
        break; //break out of while loop
      }
      else{ //if incorrect input stay in while loop
        System.out.println("Not an acceptable input please input a string"); //error message
        myScanner.next(); //clear line 
      }
    }
    while (true) //does the same just records integer instead of string
    {
      System.out.println("Please input your course number");
      boolean correctInput = myScanner.hasNextInt();
      if(correctInput)
      {
        courseNumber = myScanner.nextInt(); //record input for courseNumber
        break;
      }
      else
      {
        System.out.println("Not an acceptable input please input an integer"); //error message
        myScanner.next();
      }
    }
    while(true)
    {
      System.out.println("please input your department name.");
      boolean correctInput = myScanner.hasNext();
      
      if(correctInput)
      {
        deptName = myScanner.next(); //records input for deptName
        break;
      }
      else{
        System.out.println("Not an acceptable input please input a string");
        myScanner.next();
      }
    }
    while(true) //does the same thing but only accepts a double
    {
      System.out.println("please input your classes start time (HH.MM).");
      boolean correctInput = myScanner.hasNextDouble();
      
      if(correctInput)
      {
        startTime= myScanner.nextDouble(); //record double for startTime
        break;
      }
      else{
        System.out.println("Not an acceptable input please input a double");
        myScanner.next();
      }
    }
    while(true)
    {
      System.out.println("please input the number of times your class meets.");
      boolean correctInput = myScanner.hasNextInt();
      
      if(correctInput)
      {
        timesMet= myScanner.nextInt(); //records data for timesMet
        break;
      }
      else{
        System.out.println("Not an acceptable input please input a Integer");
        myScanner.next();
      }
    }
    while(true)
    {
      System.out.println("please input the number of students in your class.");
      boolean correctInput = myScanner.hasNextInt();
      
      if(correctInput)
      {
        numStudents= myScanner.nextInt(); //reoords data for numStudents
        break;
      }
      else{
        System.out.println("Not an acceptable input please input a Integer");
        myScanner.next();
      }
    }
    //displays all correctly inputted information
    System.out.println("You are enrolled in course number "+courseNumber);
    System.out.println("In department " + deptName);
    System.out.println("Your teacher is " + teacher);
    System.out.println("You meet "+ timesMet+" times a week");
    System.out.println("The class starts at "+ startTime);
    System.out.println("There are "+numStudents+" students in your class");
    
  }//end main method
}//end class
//Thank you for reading my code :)