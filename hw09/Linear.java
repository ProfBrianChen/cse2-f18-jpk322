import java.util.Random;
import java.util.Scanner;

public class Linear {
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        System.out.println("Please enter 15 integer inputs");
        int cnt =0;
        int[] studentGrades = new int[15];
        while(cnt<15)
        {
            boolean correctInput = scan.hasNextInt();
            if(correctInput)
            {
                int Value = scan.nextInt();
                if(Value>=0 && Value<=100)
                {
                   if(cnt==0 || studentGrades[cnt-1] <= Value)
                   {
                       studentGrades[cnt] = Value;
                       cnt++;
                   }
                   else
                   {
                       System.out.println("Please input an integer equal to or greater than the previous");
                       continue;
                   }
                }
                else
                {
                    System.out.println("Please input a valid grade between 0-100");
                    continue;
                }
            }
            else
            {
                scan.next();
                System.out.println("Please input an integer");
                continue;
            }
        }
        System.out.println("What grade do you want to search for?");
        int key = scan.nextInt();
        int keyFound = binarySearch(studentGrades,key);
        if(keyFound>=0)
        {
            System.out.println(key+" was found in the list with "+keyFound+" iterations");
        }
        else if(keyFound<0)
        {
            System.out.println(key+" was not found in the list with "+(-keyFound)+" iterations");
        }
        scramble(studentGrades);
        System.out.println("Please enter the key");
        int key2 = scan.nextInt();
        int foundDigit = LinearSearch(studentGrades,key2);
        if(foundDigit >=0)
            System.out.println(key2+" was found at position "+foundDigit);
        else
            System.out.println(key2+ " was not found in the list");

    }
    public static int binarySearch(int[] input, int key)
    {
        int min = 0;
        int cnt = 0;
        int max = input.length-1;
        while(max >= min)
        {
            int middle = (min+max)/2;
            if(key == input[middle])
                return cnt;
            else if(input[middle] < key)
                max = middle-1;
            else if(input[middle] > key)
                min = middle+1;
            cnt++;
        }
        return -cnt;
    }
    public static void scramble(int[] arr)
    {
        for(int i=0; i<1000; i++)
        {
            Random generator = new Random();
            int randNumber = generator.nextInt(arr.length);
            int temp = arr[0];
            arr[0] = arr[randNumber];
            arr[randNumber] = temp;
        }
        System.out.println("Scrambled");
    }
    public static int LinearSearch(int[] arr, int key)
    {
        for(int i=0; i<arr.length; i++)
        {
            if (arr[i] == key)
                return i;
        }
        return -1;
    }
}
