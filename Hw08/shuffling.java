/*Shuffling, printArray, getHand by Joshua Krinsky 11.12.18
* takes in an array of cards displays and modifies the array by passing it through several methods
* then deals out a poker hand picking cards from the back of the not yet chosen stack
* */

import java.util.Scanner;
import java.util.Random;
public class shuffling{
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        //suits club, heart, spade or diamond
        String[] suitNames={"C","H","S","D"};
        String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"};
        String[] cards = new String[52];
        String[] hand;
        int again = 1;
        int index = 51;
        for (int i=0; i<52; i++){
            cards[i]=rankNames[i%13]+suitNames[i/13];
            System.out.print(cards[i]+" ");
        }
        System.out.println();
        printArray(cards);
        shuffle(cards);
        printArray(cards);
        while(again == 1){
            System.out.println("How big of a hand do you want");
            int numCards = scan.nextInt();
            if(numCards >=52)
              continue;
            if (index < numCards)
            {
              shuffle(cards);
              index = 51;
              System.out.println("New deck");
              printArray(cards);
            }
            hand = getHand(cards,index,numCards);
            printArray(hand);
            index -= numCards;
            System.out.println("Enter a 1 if you want another hand drawn");
            again = scan.nextInt();
        }
    }
    public static void printArray(String[] arr) //prints the full list of the array
    {
        for(int i =0; i<arr.length; i++) //goes through every element
        {
            System.out.print(arr[i]+" "); //prints it with a space at the end
        }
        System.out.println(); //prints a new line afterward
        return;
    }
    public static void shuffle(String[] arr) //shuffles the position of the cards
    {
        int randomize = (int) (Math.random()*950+60); //number of times it will iterate through
        for(int i=0; i < randomize; i++) //runs swap
        {
            int numb = (int) ((Math.random()*arr.length-1) +1); //finds random point in array
            //swaps with the first element
            String temp = arr[numb];
            arr[numb] = arr[0];
            arr[0] = temp;
        }
        //I think this is how we were instructed to do it on the HW if not sorry ... still does randomize the list though
        return;
    }
    public static String[] getHand(String[] arr, int index, int rtnSize) //returns a hand starting from the back of the array
    {
        String[] rtnHand = new String[rtnSize]; //size of the hand
        int cnt =0;
        for(int i = index; i>index-rtnSize; i--)//picks from the last element back until hand is full
        {
            rtnHand[cnt] = arr[i];
            cnt++;
        }
        return rtnHand; //return array of cards
    }
}