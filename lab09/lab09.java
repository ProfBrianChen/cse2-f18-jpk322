/*
* By Joshua Krinsky
* 11.18.18 modifies copies and flips arrays
* 8 7 6 5 4 3 2 1
* 1 2 3 4 5 6 7 8
* 8 7 6 5 4 3 2 1*/

public class lab09 {
    public static void main(String[] args) //main method I was told how to set up
    {
        int[] array0 = {1,2,3,4,5,6,7,8};
        int[] array1 = copy(array0);
        int[] array2 = copy(array0);
        inverter(array0);
        print(array0);
        inverter2(array1);
        print(array1);
        int[] array3 = inverter2(array2);
        print(array3);

    }
    public static int[] copy(int[] input) //return a copy of the array with different memory adress
    {
        int[] rtnArr = new int[input.length];//stanard way to copy an array
        for(int i=0; i<input.length; i++)
            rtnArr[i] = input[i];
        return rtnArr;
    }
    public static void inverter(int[] input) //invers the array in a method by modifiying the array dirrectly
    {
        int n= (input.length-1)/2;
        for(int i=0; i<=n; i++)
        {
            int temp = input[i];
            input[i] = input[input.length-i-1];
            input[input.length-i-1] = temp;
        }
    }
    public static int[] inverter2(int[] input) //modify array by making a copy and changing that
    {
        int[] copy = copy(input);
        inverter(copy);
        return copy;
    }
    public static void print(int[] arr) //prints all elements in an array
    {
        for(int i=0; i<arr.length; i++)
            System.out.print(arr[i]+" ");

        System.out.println();
    }
}
