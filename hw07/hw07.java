/*By Joshua Krinsky 10.27.18
asks the user to input a body of text then gives a menu for a series of options

count the characters, count the words
find a word in the text, replace exlimation marks with periods and reduce all double or
more spaces to single spaces
*/

import java.util.Scanner; //import scanner

public class hw07 { //main class
    public static void main(String args[])
    {//main class
        String text = ""; //initialize text
        Scanner reader = new Scanner(System.in); //set up scanner
        System.out.println("Enter your String"); //ask to input a string
        text = reader.nextLine(); //take the string
        System.out.println("You entered: "+text); //output it
        while(true) //go into menu loop
        {
            String input = printMenu(); //get the input of from the menu

            if (input.equalsIgnoreCase("c")) {
                int charSpaces = NumOfNonWSChar(text);
                System.out.println("Number of non-whitespace characters: "+charSpaces); //output
            }
            else if (input.equalsIgnoreCase("w")) {
                int numWords = NumOfWords(text);
                System.out.println("Number of words: "+numWords); //output
            }
            else if (input.equalsIgnoreCase("f")) {
                System.out.println("What word do you want to search for: ");//ask for search word
                String find = reader.nextLine(); //record
                int wordsFound = findText(find,text);//run function
                System.out.println("\""+find+"\" instances: "+wordsFound);//display
            }
            else if (input.equalsIgnoreCase("r")) {
                text = replaceExclamation(text);
                System.out.println("Edited text: "+text);//output
            }
            else if (input.equalsIgnoreCase("s")) {
                text = ShortenSpace(text);
                System.out.println("Edited text: "+text);//display
            }
            else if (input.equalsIgnoreCase("q"))
                break;//quit out of program
            else{
                System.out.println("Please enter an acceptable input");//tell user invalid input
            }
        }
    }//end method
    public static String printMenu()//print the options
    {
            System.out.println("MENU");
            Scanner reader = new Scanner(System.in);//scanner for users answer
            System.out.println("c - Number of non-whitespace characters");
            System.out.println("w - Number of words");
            System.out.println("f - Find text");
            System.out.println("r - Replace all !'s");
            System.out.println("s - Shorten spaces");
            System.out.println("q - quit");
            String input = reader.nextLine(); //record

            return input;//return answer

    }//end method
    public static int NumOfNonWSChar(String text) //counts how much white space
    {
        int cnt=0; //counter
        for(int i=0; i<text.length(); i++) //runs through the text
        {
           if(text.charAt(i) != ' ') //every non space is counted
               cnt++;
        }
        return cnt;//returns the counter
    }//end method
    public static int NumOfWords(String text) //counts number of words
    {
        int cntWords =0;
        for(int i=1; i<text.length(); i++)//runs through text
            if(text.charAt(i) == ' ' && text.charAt(i-1) != ' ')//every space not preceded by a space is counted
            {
                cntWords++;
            }
            cntWords++;//plus one for the last word
        return cntWords;//return counted words
    }//end method
    public static int findText(String find, String text) //find specific string of text in the string
    {
        int i=0; //counter
        int cnt=0;//counter for number of times word appears
        while(i<text.length()-find.length()) //run until the text - search word
        {
            String check = text.substring(i,i+find.length()); //get section of text
            if(find.equalsIgnoreCase(check))//check if this section equals search word
                cnt++;
            i++; //increase
        }
        return cnt; //return times the word is found
    }//end method
    public static String replaceExclamation(String text){ //replace all ! with .
        String rtn_string = text.replaceAll("!","."); //simple string function
        return rtn_string; //return new string
    }//end method
    public static String ShortenSpace(String text) //take all spaces 2 or longer and make them one
    {
        String trimmedText = text.trim(); //trim the space from the front and back of the string
        int i =1; 
        while(i<trimmedText.length()) //run through each character
        {
            if(trimmedText.charAt(i) == ' ' &&trimmedText.charAt(i-1) == ' ') //check if character before is a space and that current character is a space indicates that its two spaces in a row
                trimmedText = trimmedText.substring(0,i)+trimmedText.substring(i+1,trimmedText.length()); //append two substrings leaving out the extra space
            else
                i++; //ony incriment if not true inorder to not skip over triple or quadruple spaces
        }
        return trimmedText;
    }//end method
}//end class
