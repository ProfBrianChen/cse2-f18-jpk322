/* By Joshua Krinsky 9.14.18
the purpose of this programi s to take the acres effected by a hurricaine
and the inches of rainfall on average and then convert the acres
to output the average cubic miles of rainfall

*/
import java.util.Scanner; //imports scanner
public class Convert{
  public static void main(String []args) //main method
  {
    Scanner reader = new Scanner(System.in); //declare scanner
    System.out.print("Enter area effected in acres: "); //prompt user t enter acres effected by hurricaine
    double acresEffected = reader.nextDouble(); //record input
    
    System.out.print("Enter the rainfall in the effected area: "); //prompt user to enter amount of average rainfall
    double rainfall = reader.nextDouble(); //record users input
    
    final double ACRES_PER_MILE = 640; //number of Acres/mile
    final double INCHES_PER_MILE = 63360; //number of inches/mile
    
    double milesSquared = acresEffected/ACRES_PER_MILE; //convert arces to miles squared
    double mile = rainfall / INCHES_PER_MILE; //convert inches to miles
    
    double cubicMiles = milesSquared * mile; //multiply mile and mile sqared to get cubic miles
    
    System.out.println(cubicMiles+" cubic miles"); //display cubic miles of average rainfall
    
  } //end main method
}//end class