/*By Joshua Krinsky 9.14.18
calculates the volume of a pyramid
the user inputs Width and height (2 doubles)
and the output is the volume of the pyramid
*/


import java.util.Scanner; //imports scanner
public class Pyramid{
  public static void main(String [] args) //main method
  {
    Scanner reader = new Scanner(System.in); //declares scanner
    System.out.print("The square side of the pyramid is (input length): "); //prompts user for width
    double width = reader.nextDouble(); //records input
    
    System.out.print("The height of the pyramid is (input height): "); //propmts user for height
    double height = reader.nextDouble(); //records input
    
    //calculates the volume of the pyramid
    double volume = (height * width*width)/3.0;
    
    System.out.println("The volume inside the pyramid is: "+volume+"."); //display the pyramid's volume
  } //end main method
}//end class