/*By joshua krinsky
Takes user input as integer between 0 - 100 
and assignes it the length of a square
and then inscribes an X in the square
ex: 6
 *****
* *** *
** * **
*** ***
** * **
* *** *
 *****
*/
import java.util.Scanner; //import scanner

public class EncryptedX { //begin class
    public static void main(String args[])
    { //begin main method
        Scanner reader = new Scanner(System.in); //declare scanner
        int size; //size of legnth
        while(true) { //start while loop
            System.out.println("What is the size of the square?"); //input the length
            boolean correctInput = reader.hasNextInt(); //check int is used

            if (correctInput) {
                size = reader.nextInt();
                if(size <= 100 && size >= 0)//check int is within bounds
                {
                    break;
                }
                else{//err statement if int not inbounds
                    System.out.println("Please input an integer bewtween 0 and 100");
                }
            }
            else{ //err statement if int not inputted
                System.out.println("Please input an integer between 0 and 100");
                reader.nextLine();
            }
        }
        createX(size);//display the X

    }//end main method
    public static void createX(int size)//method used to display X
    {
        for(int i=0; i<=size; i++) //number of rows according to size
        {
            for(int j=0; j<=size; j++) //number of coloumbs according to size
            {
                if(j==i || j == size - i) //use a space when j is equal to i or is equal to size minues i minus 1
                {
                    System.out.print(" ");
                }
                else
                {//otherwise print a star
                    System.out.print("*");
                }
            }
            System.out.println(); //newline
        }
        return; //returns statment
    }//end method
} //end class
