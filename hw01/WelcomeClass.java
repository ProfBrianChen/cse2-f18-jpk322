//By joshua Krinsky 9.1.18

// display welcome with a order
/* display my netwrok ID JPK322 with fancy lining
up carrots on one row
dashes on the over then displays the ID mixed with -
lastly reversing the order of the dashes and then displaying Vs acroos the bottom
finally I end with my autobiography
*/

public class WelcomeClass{
  //class called WelcomeClass
  public static void main(String args[])
  {
    //welcome message
    System.out.println("-----------");
    System.out.println("| WELCOME |");
    System.out.println("-----------");
    System.out.println("  ^  ^  ^  ^  ^  ^  ");
    // prints  / \/ \/ \/ \/ \/ \ because you need to use an escape character 
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\ ");
    System.out.println("<-J--P--K--3--2--2->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ / ");
    System.out.println("  v  v  v  v  v  v  ");
    //added and extra line the whole display just felt cluttered
    System.out.println();
    //my autobiography
    System.out.println("Hello my name is Joshua Krinsky I have");
    System.out.println("been programming for a few years now. I was born in Melrose, Ma");
    System.out.println("but moved to andover when I was ten. Finally I went to AHS");
    System.out.println("focussing on my studies getting goud enough grades to go to");
    System.out.println("Lehigh University where I am majoring in CSB");
    System.out.println();
  }
}