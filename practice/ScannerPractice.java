import java.util.Scanner;

public class ScannerPractice{
  
  public static void main (String []args)
  {
    Scanner reader = new Scanner(System.in);
    
    System.out.println("Please input integer A");
    int a = reader.nextInt();
    
    System.out.println("please input integer B");
    int b = reader.nextInt();
    
    System.out.println("A: "+a);
    System.out.println("B: "+b);
    System.out.println("A + B = "+ (a+b));
    reader.nextLine();
    System.out.println("Please input your name");
    String name = reader.nextLine();
    System.out.println("Hello, "+name);
  }

}