public class loopPractice{
  public static void main(String args[])
  {
    int i =10; //sentinel - ending number to insure no infinite loop
    int j =0;
    while(j <= i)
    {
     System.out.printf("The variable j = %d.\n",j);
      j++;
    }
    System.out.printf("j terminated at the value %d.\n",j);
  }
}