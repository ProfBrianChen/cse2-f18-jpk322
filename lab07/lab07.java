/*By Joshua Krinsky 10.27.18
* code asks the user if they want a sentence displayed
* then proceeds to output 3 related sentences by randomly getting
* verbs, adjectives and object noun for each one
* but relating to the same subject verb
*  */

import java.util.Random; //import random
import java.util.Scanner; //import scanner

public class lab07 { //start class
    public static void main(String args[]) //start main method
    {
        while(true)
        {
            Scanner reader = new Scanner(System.in); //declare scanner
            System.out.println("Would you like to construct a sentence yes/no"); //prompt user
            String input = reader.nextLine(); //record input
            if(input.equalsIgnoreCase("yes")) //if typed yes
            {
                String rtn = paragraph(); //run paragraph
                System.out.println(rtn); //print returned statement
            }
            else //break out of loop
                break;
        }
    }//end main method
    public static String Verb() //returns a random verb from 0 - 9
    {
        Random generator = new Random(); //declare random number generator
        int verbInt = generator.nextInt(9); //gets number
        switch(verbInt) //determine and return verb
        {
            case 1:
                return "passed";
            case 2:
                return "beat";
            case 3:
                return "created";
            case 4:
                return "poked";
            case 5:
                return "invented";
            case 6:
                return "annoyed";
            case 7:
                return "kicked";
            case 8:
                return "angered";
            default:
                return "yeeted";
        }
    }
    public static String Adjective() //same function as verb but w/ adjective
    {
        Random generator = new Random();
        int verbInt = generator.nextInt(9);
        switch(verbInt)
        {
            case 1:
                return "lazy";
            case 2:
                return "happy";
            case 3:
                return "joyful";
            case 4:
                return "brave";
            case 5:
                return "calm";
            case 6:
                return "delightful";
            case 7:
                return "eager";
            case 8:
                return "ambitious";
            default:
                return "yeet-like";
        }
    }
    public static String subjectNoun() //same function as verb but w/ subject nuon
    {
        Random generator = new Random();
        int verbInt = generator.nextInt(9);
        if(verbInt == 0)
            return "dog";
        else if(verbInt == 1)
            return "cat";
        else if(verbInt == 2)
            return "person";
        else if(verbInt == 3)
            return "community";
        else if(verbInt == 4)
            return "nobleman";
        else if(verbInt == 5)
            return "queen";
        else if(verbInt == 6)
            return "policeman";
        else if(verbInt == 7)
            return "elephant";
        else if(verbInt == 8)
            return "ant-easter";
        else
            return "yeeter";
    }
    public static String objectNoun() //same function as verb but w/ object nuon
    {
        Random generator = new Random();
        int verbInt = generator.nextInt(9);
        if(verbInt == 0)
            return "fox";
        else if(verbInt == 1)
            return "chicken";
        else if(verbInt == 2)
            return "cow";
        else if(verbInt == 3)
            return "farm";
        else if(verbInt == 4)
            return "planet";
        else if(verbInt == 5)
            return "postman";
        else if(verbInt == 6)
            return "fireman";
        else if(verbInt == 7)
            return "fur-trader";
        else if(verbInt == 8)
            return "antiquities dealer";
        else
            return "yeeter";
    }
    public static String firstSentence(String subjNoun) //returns a constructed sentence with input of a subject noun
    {
        String verb = Verb();
        String adjective2 = Adjective();
        String adjective = Adjective();
        String objNoun = objectNoun();

        return " The "+ adjective +" "+subjNoun+" "+verb+" the "+adjective2+" "+objNoun;
    }
    public static String paragraph() //returns a paragraph of three related sentences(related by subject noun)
    {
        String subjNoun = subjectNoun();
        String sentenceOne= firstSentence(subjNoun);
        String sentenceTwo= firstSentence(subjNoun);
        String conclusion = firstSentence(subjNoun);

        return sentenceOne + sentenceTwo + conclusion; //returns the paragraph
    }//end method
}//end class
