import java.util.Scanner;

public class TicTacToe {
    public static void main(String[] args)
    {
        String[][] Grid = new String[3][3];
        resetBoard(Grid);
        Scanner reader = new Scanner(System.in);
        while(true)
        {
            printBoard(Grid);
            while(true) {
                System.out.println("O, Please enter a position for your next move");
                boolean correctInput = reader.hasNextInt();
                if(correctInput) {
                    int val = reader.nextInt();
                    if (updateBoard(Grid, val, "O"))
                        break;
                }
            }
            printBoard(Grid);
            if(checkWinner(Grid,"O")) {
                System.out.println("Congrats O you have one");
                break;
            }

            if(checkDraw(Grid))
            {
                System.out.println("It was a draw");
                break;
            }
            while(true) {
                System.out.println("X, Please enter a position for your next move");
                boolean correctInput = reader.hasNextInt();
                if(correctInput) {
                    int val = reader.nextInt();
                    if (updateBoard(Grid, val, "X"))
                        break;
                }
            }
            if(checkWinner(Grid,"X")) {
                System.out.println("Congrats X you have one");
                break;
            }
        }
    }
    public static void resetBoard(String[][] grid)
    {
        int cnt =1;
        for(int i=0;i<grid.length; i++)
        {
            for(int j=0; j<grid.length; j++)
            {
                grid[i][j] = cnt+"";
                cnt++;
            }
        }
    }
    public static void printBoard(String[][] grid)
    {
        for(int i=0; i<grid.length; i++)
        {
            for(int j=0; j<grid.length; j++)
            {
                System.out.print(grid[i][j]+" ");
            }
            System.out.println();
        }
    }
    public static boolean updateBoard(String[][] grid, int key, String type)
    {
        String stringKey = key+"";
        for(int i=0; i<grid.length; i++) {
            for(int j=0; j<grid.length; j++) {
                if (grid[i][j].equals(stringKey)) {
                    grid[i][j] = type;
                    return true;
                }
            }
        }
        return false;
    }
    public static boolean checkWinner(String[][] grid, String type)
    {
        for(int i=0; i<grid.length; i++)
        {
            if(grid[i][1].equals(grid[i][2]) && grid[i][2].equals(grid[i][0]))
                return true;
        }
        for(int i=0; i<grid.length; i++)
        {
            if(grid[1][i].equals(grid[2][i]) && grid[2][i].equals(grid[0][i]))
                return true;
        }
        if(grid[0][0].equals(grid[1][1]) && grid[1][1].equals(grid[2][2]))
            return true;
        if(grid[2][0].equals(grid[1][1]) && grid[1][1].equals(grid[0][2]))
            return true;
        return false;
    }
    public static boolean checkDraw(String[][] grid)
    {
        for(int i=0; i<grid.length; i++)
        {
            for(int j=0; j<grid.length; j++)
            {
              if(!grid[i][j].equals("O") && !grid[i][j].equals("X"))
                  return false;
            }
        }
        return true;
    }

}
